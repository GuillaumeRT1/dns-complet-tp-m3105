options {
	directory "/etc/named";
};

zone "." {
	type hint;
	file "named.root";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};

zone "wiki.org" {
 	type master;
 	file "wiki.dir";
};

zone "7.0.10.IN-ADDR.ARPA" {
 	type master;
 	file "wiki.rev";
};








$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org. (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)

.        	3600000 IN NS aRootServer.
aRootServer. 	3600000    A  10.0.0.10











$TTL	86400
@	IN	SOA	localhost. root.localhost (
	20041128 ; Serial
	28800	 ; Refresh
	7200	 ; Retry
	3600000	 ; Expire
	86400 	 ; Minimum
)
	IN	NS	localhost.
1 	IN 	PTR 	localhost.








$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)
wiki.org.   IN  NS  	dwikiorg.wiki.org.
wiki.org.   IN  A  	10.0.7.10
dwikiorg    IN  A   	10.0.7.10
www         IN  A       10.0.7.11
gateway     IN  A   	10.0.7.1









$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)
@	    IN  NS  	dwikiorg.wiki.org.
1	    IN	PTR	gateway.wiki.org.
10          IN  PTR   	dwikiorg.wiki.org.
11          IN  PTR	www.wiki.org.








