#!/usr/bin/env bash

## On commence par kill tous les serveurs
himage dwikiorg    killall -9 -q named
himage diutre      killall -9 -q named
himage dorg        killall -9 -q named
himage dre         killall -9 -q named
himage aRootServer killall -9 -q named

## Ici nous avons la configuration de drtiutre
# configuration du serveur drtiutre

himage drtiutre mkdir -p /etc/named
hcp drtiutre/named.conf drtiutre:/etc/.
hcp drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf
## Ici on lance le serveur
himage drtiutre named -c /etc/named.conf


## Ici nous avons la configuration de wiki.org
# configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp dwikiorg/named.conf dwikiorg:/etc/.
hcp dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf
## Ici on lance le serveur
himage dwikiorg named -c /etc/named.conf

## Ici nous avons la configuration de iut.re
# configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp diutre/named.conf diutre:/etc/.
hcp  diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf
## Ici on lance le serveur
himage diutre named -c /etc/named.conf

## Ici nous avons la configuration de dorg
# configuration du serveur dns

himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf
## Ici on lance le serveur
himage dorg named -c /etc/named.conf


## Ici nous avons la configuration de dre
# configuration du serveur dns

himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf
## Ici on lance le serveur
himage dre named -c /etc/named.conf

## Ici nous avons la configuration de aRootServer
# configuration du serveur aRootServer

himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf
## Ici on lance le serveur
himage aRootServer named -c /etc/named.conf
